function showCategory(categoryNumber) {
  var plusButton = document.getElementsByClassName("plus")[categoryNumber];
  var minusButton = document.getElementsByClassName("minus")[categoryNumber];
  var showQuestion = document.getElementsByClassName("question_list")[categoryNumber];
  
  if (showQuestion.style.display == "block") {
    minusButton.style.display = "none";
    plusButton.style.display = "block";
    showQuestion.style.display = "none";
  } else {
    minusButton.style.display = "block";
    plusButton.style.display = "none";
    showQuestion.style.display = "block";
  }
};